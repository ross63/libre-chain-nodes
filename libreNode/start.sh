#!/bin/bash
################################################################################
# Libre tools 
# Originated from scripts by by CryptoLions.io
###############################################################################
source /opt/libre-chain-nodes/libreNode/node.env

$MAINDIR/stop.sh
echo -e "Starting Nodeos \n";

ulimit -n 65535
ulimit -s 64000

$NODEOSBINDIR/nodeos --data-dir $DATADIR --config-dir $MAINDIR "$@" > $DATADIR/stdout.txt 2> $DATADIR/stderr.txt &  echo $! > $DATADIR/nodeos.pid