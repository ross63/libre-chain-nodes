#!/bin/bash
# producers can set resources for other accounts
# setalimits script for setting RAM, CPU, NET of accounts
AUTH="eosio" #authorization account
ACCT="node1" #account to set limits on
NET="2"
CPU="2"
RAM_IN_MB="1"
RAM=$(( $RAM_IN_MB * 1024*1000 ))
#RAM="1024000"
cleos.sh push action eosio setalimits '{"authorizer":"'"$AUTH"'","account":"'"$ACCT"'","ram":"'"$RAM"'","net":"'"$NET"'","cpu":"'"$CPU"'"}' -p $AUTH@active
